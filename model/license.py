from model.method import Method


class License(Method):

    def __init__(self):
        Method.__init__(self)
        self.set_method('flickr.photos.licenses.getInfo')

    def get_info(self):
        return self.get()

    def get_description(self, license_id):
        response = self.get_info()

        if response.status_code == 200:
            response_dictionary = response.json()
            if response_dictionary['stat'] == 'ok':
                if 'licenses' in response_dictionary:
                    licenses = response_dictionary['licenses']

                    if 'license' in licenses:
                        license = licenses['license']

                        for license_dictionary in license:
                            if license_dictionary['id'] == license_id:
                                return license_dictionary
