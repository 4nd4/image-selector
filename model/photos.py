import time
import datetime
import os
import configuration
from model.db_image import Database
from model.logs import Logs
from model.method import Method
from model.url_functions import url_string_from_dictionary


class Photos(Database, Method):

    def __init__(self):
        Database.__init__(self, "YAHOO_REST")
        Method.__init__(self)

    def get_last_epoch(self, age):
        # count_cursor = self.get_cl_data().find({'age': age}).sort([('dates.posted', -1)]).limit(1).count()

        # check in log table, if nothing found, check posted date on data table, else set to today.
        # do this so the script doesn't have to run through the entire dates again

        log = Logs()

        log_timestamp = log.get_unix_datetime_checked(age)

        if log_timestamp is None:
            count_cursor = self.get_cl_data().find({'age': age}).sort([('dates.posted', 1)]).limit(1).count()

            if count_cursor == 0:
                return int(time.time())
            else:
                last_record_cursor = self.get_cl_data().find({'age': age}).sort([('dates.posted', 1)]).limit(1)

                for rec in last_record_cursor:
                    return rec['dates']['posted']

        else:
            return log_timestamp

    def get_first_epoch(self, age):

        # get last record updated by age, if it's bigger than 1 month, set to 1 month

        count_cursor = self.get_cl_data().find({'age': age}).sort([('_id', 1)]).limit(1).count()

        # count_cursor = self.get_cl().find({'age': age}).sort([('dates.posted', 1)]).limit(1).count()

        if count_cursor == 0:
            return int(time.time())
        else:
            first_record_cursor = self.get_cl_data().find({'age': age}).sort([('_id', 1)]).limit(1)

        for rec in first_record_cursor:
            generation_time = rec['_id'].generation_time

            return generation_time

    def search(
            self,
            age,
            text,
            ranges,
            license,
            media,
            extras,
            # is_commons,
            page,
            download=None
            # number_images

    ):
        # count images from database

        counter = self.get_cl_data().find(
            {
                'age': age
            }
        ).count()

        print(age, counter)

        self.set_method('search')

        args_dict = {
            # 'tag_mode': tag_mode,
            # 'tags': tags,
            'text': text,
            'min_upload_date': ranges[0],  # change so you get 4k images
            'max_upload_date': ranges[1],
            'license': license,
            'media': media,  # photos or videos
            'extras': extras,
            # 'is_commons': is_commons,  # only gives images with license 7 (No known copyright restrictions)
            'per_page': 500,
            'page': page
        }

        resp = self.get(url_string_from_dictionary(args_dict))

        if resp.status_code != 200:
            print('Error', resp.status_code)
            return False
        else:

            record = resp.json()
            
            if record is None:
                print('record returned None', ranges)
                return False

            s_number_pages = record['photos']['pages']
            s_total_images = record['photos']['total']

            number_pages = int(s_number_pages) if s_number_pages is not None else 0
            total_images = int(s_total_images) if s_total_images is not None else 0

            print('number pages', number_pages)
            print('page', page)
            print('total', total_images)

            threshold = 500
            flickr_max = 4000

            # Flickr only allows 4000.... it must be filtered by min_upload_date

            # todo: here insert to log

            print('DATE:', datetime.datetime.fromtimestamp(
                    int(ranges[0])
                ).strftime('%Y-%m-%d %H:%M:%S'),
                datetime.datetime.fromtimestamp(
                    int(ranges[1])
                ).strftime('%Y-%m-%d %H:%M:%S')
            )

            if total_images > flickr_max:

                print('INFO: adding a day to lessen results...')

                ranges[0] += 86400

                # reduce a day 86400

                self.search(age, text, ranges, license, media, extras, page, download)

            if page > number_pages:
                print('page is greater than available (no data found)')
                return False
            else:

                photos = record.get('photos')

                if photos is not None:

                    photo_array = photos.get('photo')

                    for p in photo_array:

                        # check if item is already in database or already generated

                        # subject_id here is not an object id

                        subject_id = p.get('id')

                        check_item = self.get_cl_data().find_one(
                            {
                                'id': subject_id
                            }
                        )

                        if not check_item:

                            p.update(
                                {
                                    'age': age,
                                }
                            )

                            print p

                            info = self.get_info(p.get('id'))

                            if info is not None:

                                photo = info.get('photo')

                                if photo is not None:
                                    photo = info.get('photo')

                                    dict_merge = dict(p.items() + photo.items())

                                    #

                                    # check in image_create, if it exists, data must have been deleted!

                                    _id = self.get_cl_data().insert(dict_merge)

                                    counter += 1

                                    if download:

                                        db_class = configuration.yahoo_db
                                        db_class.set_root_path(configuration.path_directory_creation)
                                        db_class.get_metadata_download(_id)

                                else:
                                    print('Error: unable to merge photo info', p.get('id'))

                                    return True

                            else:
                                print('Error: obtaining info', p.get('id'))

                        else:

                            # check deleted metadata

                            # this is true, todo: possibly store in another table that it has already been analysed

                            print('item {0}, already in db'.format(subject_id))    #fsanda

                if page == number_pages:
                    return True

                # self.search(age, tags, tag_mode, is_commons, page + 1, number_images)

                self.search(age, text, ranges, license, media, extras, page + 1, download)

    def get_all_contexts(self, photo_id):

        self.set_method('getAllContexts')

        args_dictionary = {'photo_id': photo_id}

        response = self.get(args=url_string_from_dictionary(args_dictionary))

        return response

    def get_info(self, photo_id):

        self.set_method('getInfo')

        args_dictionary = {'photo_id': photo_id}

        response = self.get(args=url_string_from_dictionary(args_dictionary))

        if response.status_code != 200:
            print('Error: get_info', response.status_code)

            # pause and try again

            print('retrying in 5 seconds...')
            time.sleep(5)

            response = self.get(args=url_string_from_dictionary(args_dictionary))

            if response.status_code == 200:
                print('reattempt successful')
                return response.json()
            else:
                print('Error: reattempt unsuccessful', photo_id)
        else:
            return response.json()


def save_document(_id, dict_photo, type):
    # save html

    if 'urls' in dict_photo:
        urls = dict_photo['urls']

        if 'url' in urls:
            url = urls['url']

            for u in url:
                if '_content' in u:
                    content_url = str(u['_content'])
                    # print content_url
                    # response = urllib2.urlopen(content_url)
                    # web_content = response.read()

                    # save html

                    # TODO: implement but might delete capability
                    if type == 'html':
                        document_path = configuration.document_path + type + '/' + str(_id) + '.' + type
                        # with open(document_path, 'w') as f:
                        # f.write(web_content)

                        # soup = bs4.BeautifulSoup(web_content, 'lxml')

                        # img = soup.find('meta', property='og:image')

                        # img_url = img.attrs['content']
                        # img_path = urlparse.urlparse(img_url).path
                        # img_path = configuration.document_path + 'html/' + 'c1.staticflickr.com/' + img_path

                        # os.makedirs(os.path.dirname(img_path))

                        # img = urllib2.Request(img_url)
                        # img_data = urllib2.urlopen(img).read()

                        # with open(img_path, 'wb') as f:
                        #    f.write(img_data)

                        return True

                    # save pdf

                    if type == 'pdf':
                        document_path_pdf = os.path.expanduser(
                            configuration.document_path + type + '/' + str(_id) + '.' + type)

                        path_wkthmltopdf = configuration.path_wkthmltopdf


                        # todo commented because not using
                        #config = pdfkit.configuration(
                        #    wkhtmltopdf=path_wkthmltopdf
                        #)

                        # to remove browser incompatibility

                        options = {
                            'custom-header': [('User-Agent',
                                               'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36')],
                            '--custom-header-propagation': None
                        }
                        # todo commented because not using
                        #pdfkit.from_url(url=content_url, output_path=document_path_pdf, options=options,
                        #                configuration=config)
                else:
                    print('Error: _content', 'not available')
                    return False
        else:
            print('No url')
            return False
    else:
        print('No URLS... gg')
        return False

    class Licenses(Photos):
        def get_info(self):
            self.set_method('getInfo')

            # args_dictionary = {'photo_id': photo_id}

            response = self.get(args=url_string_from_dictionary())

            if response.status_code != 200:
                print('Error: Licenses get_info', response.status_code)
            else:
                return response.json()
