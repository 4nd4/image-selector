from model.db_image import Database
from model.method import Method


class Logs(Database, Method):

    def __init__(self):
        Database.__init__(self, "YAHOO_REST")
        Method.__init__(self)

    def insert_log(self, age, date):

        self.get_cl_log().insert_one(
            {
                'age': age,
                'date_checked': date
            }
        )

    def get_unix_datetime_checked(self, age):

        count_cursor = self.get_cl_log().find({'age': age}).sort([('date_checked', -1)]).limit(1).count()

        if count_cursor > 0:

            first_record_cursor = self.get_cl_log().find({'age': age}).sort([('date_checked', -1)]).limit(1)

            for rec in first_record_cursor:
                return rec['date_checked']

            return


