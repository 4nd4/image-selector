from pymongo import MongoClient
from bson.objectid import ObjectId


# todo change source

client = MongoClient()

cl_deleted = client["DELETE_YAHOO"]["data"]


class Database:

    def __init__(self, name):

        self.name = name
        self.root_path = ''
        self.crop_path = ''
        self.database = client[name]
        self.cl_data = self.database['data']
        self.cl_log = self.database['log']
        self.cl_image_created = self.database["image_created"]
        self.cl_deleted = cl_deleted
        self.image_path = None

    def get_cl_log(self):
        return self.cl_log

    def get_image_path(self):
        print(self.image_path, self.get_name())  # is useful for debugging
        return self.image_path

    def set_root_path(self, path):
        self.root_path = path

    def set_crop_path(self, path):
        self.crop_path = path

    def get_name(self):
        return self.name

    def get_root_path(self):
        return self.root_path

    def get_crop_path(self):
        return self.crop_path

    def get_cl_data(self):
        return self.cl_data

    def get_random(self):
        return self.r

    def get_cl_image_created(self):
        return self.cl_image_created

    def get_delete(self):
        return self.cl_deleted

    def delete_random_documents(self):
        self.get_random().drop()

    def get_deleted_list(self):

        list_deleted = []

        deleted = self.get_delete().find({}, {"id": 1})

        for i in deleted:
            list_deleted.append(i['id'])

        return list_deleted

    def delete_file(self, object_id):
        self.get_delete().insert(
            {'id': ObjectId(object_id)}
        )

    def get_processed_list(self):
        list_processed = []
        #processed = self.get_random().find({}, {"id": 1})
        processed = self.get_cl_image_created().find({}, {"id": 1})

        for i in processed:
            list_processed.append(i['id'])

        return list_processed

    def get_record_eval(self, list_filtered):

        cursor = self.get_db().find(
            {
                '_id': {"$nin": list_filtered}
                # not in kairos, amazon, etc and not deleted
            }, no_cursor_timeout=True
        )

        return cursor

    def count_image_face_recognition(self, age):
        list_images_created = self.get_processed_list()

        record = self.get_cl_data().find(
            {
                '$and': [{'age': age}, {'face_recognition': {'$exists': False}}, {'_id': {"$in": list_images_created}}]
            }
        )

        return record.count()

    def get_record_image_face_recognition(self, age):
        # 1) get all image_created id field in a list of ObjectId's,
        # 2) query the data table where face_recognition doesn't exist and _id in list created from 1)

        list_images_created = self.get_processed_list()

        record = self.get_cl_data().find_one(
            {
                '$and': [{'age': age}, {'face_recognition': {'$exists': False}}, {'_id': {"$in": list_images_created}}]
            }
        )

        return record

    def manage_paths(self, record):
        """
Manages the path output due to it's differences according to the database used.
For WIKI and IMDB it is slightly different because it could either be in the normal folder,
the cropped folder or neither
        :param record: row from database
        :return: full path
        """
        return record['full_path']

    def insert_face_recognition(self, subject_id, pipe):
        self.get_cl_data().update(
            {
                '_id': ObjectId(subject_id)
            },
            {
                "$set": pipe
            }
        )

    def check_face_recognition(self, subject_id):

        record = self.get_cl_data().find_one({
            '_id': ObjectId(subject_id),
        })

        if record:
            if 'face_recognition' in record:
                return record
        else:
            print('no record registered with', subject_id)


