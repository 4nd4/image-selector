import requests
import configuration


class Method:
    def __init__(self):
        self.method = None
        self.api_key = configuration.flickr_api_key
        self.api_secret = configuration.flickr_api_secret
        self.endpoint = 'https://api.flickr.com/services/rest/'
        self.child_class = self.__class__.__name__

    def set_method(self, method):
        # get instance / class

        child_class = str(self.__class__.__name__).lower()

        self.method = 'flickr.' + child_class + '.' + method

    def get_method(self):
        return self.method

    def get_api_key(self):
        return self.api_key

    def get_api_secret(self):
        return self.api_secret

    def get_endpoint(self):
        return self.endpoint

    def get(self, args=None):

        if args is None:
            args = ''

        return requests.get(
            self.get_endpoint() +
            "?method=" + self.get_method() +
            '&api_key=' + self.get_api_key() +
            '&api_secret=' + self.get_api_secret() +
            args +
            '&format=json' +
            '&nojsoncallback=1'
        )