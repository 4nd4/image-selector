import urllib2
import urlparse

import os

import bs4
import pdfkit

import configuration
from model.db_image import Database
from model.method import Method
from model.url_functions import url_string_from_dictionary


class Tags(Method):

    def __init__(self):
        Method.__init__(self)

    def get_list_photo(self, photo_id):

        self.set_method('getListPhoto')

        args_dictionary = {'photo_id': photo_id}

        response = self.get(args=url_string_from_dictionary(args_dictionary))

        if response.status_code != 200:

            print('Error get_list_photo', response.status_code)
        else:
            return response.json()
