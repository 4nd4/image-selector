def url_string_from_dictionary(params_dict):
    return "".join(["=".join(['&' + key, str(val)]) for key, val in params_dict.items()])