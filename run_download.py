import configuration
import sys
from controller import cn_image
from multiprocessing.dummy import Pool as ThreadPool

images = cn_image.Query(configuration.db_list, configuration.path_directory_creation)


def generate_images_metadata(age):
    images.set_minimum_age(age)
    images.set_maximum_age(age)

    images.process()


def download_all(age_default):
    if len(sys.argv) == 1:
        age_param = age_default
        print('executing default age', age_default)

    else:
        age_param = int(sys.argv[1])

    generate_images_metadata(age_param)

#################################################################
# run code  #
#################################################################

# __MAIN__

# generates images on db and physical


download_all(1)

# make the Pool of workers
#pool = ThreadPool(20)

#pool.map(download_all, range(30, 31))

print('Done')
