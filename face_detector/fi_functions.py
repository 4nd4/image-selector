import shutil

import os

import configuration


def copy_file(age, file_path, number_faces):

    root_path = os.path.expanduser(configuration.path_copy_images)

    file_name = os.path.basename(file_path)

    if number_faces == 0:
        folder_name = 'others'

    elif number_faces == 1:
        folder_name = 'single'
    else:
        folder_name = 'multiple'

    destination_directory = root_path + "/{0}/{1}/{2}".format(age, folder_name, file_name)

    destination_directory_root = os.path.dirname(destination_directory)

    if not os.path.exists(destination_directory_root):
        os.makedirs(destination_directory_root)

    print('copying to', destination_directory)

    shutil.copy2(file_path, destination_directory)
