import datetime
import time
import calendar
import sys
from birthday_dictionary import filter_one_dictionary
from model.photos import Photos
from multiprocessing.dummy import Pool as ThreadPool

p = Photos()


def has_number(text):
    return any(char.isdigit() for char in text)


def get_ordinal_indicator(list_numbers):
    for v in list_numbers:
        if (
                str(v).endswith('st') or
                str(v).endswith('nd') or
                str(v).endswith('rd') or
                str(v).endswith('th')
        ) and has_number(v):
            return v


def get_babies(baby_text):
    epoch_now = int(p.get_last_epoch(0))

    years = 1
    minutes = 60
    seconds = 60
    hours = 24
    days = 30

    epoch_before = epoch_now - (86400 * 30)

    ranges = [epoch_before, epoch_now]

    licenses = '1,2,3,4,5,6,7,8,9,10'

    import calendar

    dt = datetime.datetime(1990, 01, 01, 0, 0)

    min_unixtime = calendar.timegm(dt.utctimetuple())

    try:

        while epoch_now > min_unixtime:
            p.search(
                age=0,
                # tags='ten, tenth, 10th',
                # tag_mode='any',     #all [AND] or any [OR]
                text=baby_text,
                ranges=ranges,
                license=licenses,
                media='photos',
                extras='url_o',
                # is_commons=False,
                page=1,
                # number_images=6000
            )

            epoch_now = ranges[0]
            epoch_before = ranges[0] - (86400 * 30)

            ranges = [epoch_before, epoch_now]

    except KeyboardInterrupt:
        print('interrupted')


def get_all(age_default, update, download=None):
    """
Gets all metadata from the date it was interrupted or until the closest to 2000.
If update is set to true, it evaluates from today back until it wasn't evaluated
    :param download:
    :param age_default:
    :param update:
    """
    # get current unix epoch

    # last posted date

    if len(sys.argv) == 1:
        start_age = age_default
        print('using default age because you sent no parameter', age_default)
    else:
        start_age = int(sys.argv[1])

    try:

        values = filter_one_dictionary[start_age]

        print('#' * 20)
        print('age', start_age)

        if not update:
            epoch_now = int(p.get_last_epoch(start_age))

            time_window = 86400 * 30
            dt = datetime.datetime(2000, 01, 01, 0, 0)
            min_unixtime = calendar.timegm(dt.utctimetuple())

            print('proceeding to resume collection ......')

        else:
            # today
            print('proceeding to update ......')
            epoch_now = int(time.time())

            time_window = 86400 * 1    # check with a frame of 1 day
            min_unixtime = calendar.timegm(p.get_first_epoch(start_age).utctimetuple())

        readable_epoch_now = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(epoch_now))
        readable_epoch_to = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(min_unixtime))

        print('attempting to retrieve records from {0} to {1}'.format(readable_epoch_now, readable_epoch_to))

        epoch_before = epoch_now - time_window

        ranges = [epoch_before, epoch_now]

        # 0 is all rights reserved

        licenses = '1,2,3,4,5,6,7,8,9,10'

        ordinal_indicator = get_ordinal_indicator(values)

        birthday_text = ordinal_indicator + " " + 'birthday'

        # log = Logs()

        while epoch_now > min_unixtime:

            print(readable_epoch_now)

            # store date in log for age

            p.search(
                age=start_age,
                # tags='ten, tenth, 10th',
                # tag_mode='any',     #all [AND] or any [OR]
                text=birthday_text,
                ranges=ranges,

                license=licenses,
                media='photos',
                extras='url_o',
                # is_commons=False,
                page=1,
                # number_images=6000
                download=download
            )

            # todo log.insert_log(start_age, epoch_now)

            epoch_now = ranges[0]
            epoch_before = ranges[0] - time_window

            ranges = [epoch_before, epoch_now]

    except KeyboardInterrupt:
        print('interrupted')


def thread_get_all(age_default):
    get_all(age_default=age_default, update=True, download=True)


#################################################################
# run code  #
#################################################################

# get_babies('newborn')

age = 100

# updates records first
# get_all(age_default=age, update=True)
# resumes history records
if False:
    get_all(age_default=age, update=True, download=True)

if True:
    pool = ThreadPool(20)
    pool.map(thread_get_all, range(8, 101))

print('Done')