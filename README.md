# README #

This project retrieves metadata from Flickr and downloads the images by executing 2 scripts. The first script is run_metadata.py and accepts an age as a parameter, it will store the metadata in MongoDB. The second script is run_download.py and accepts an age as a parameter, it will download the image to the disk

### What is this repository for? ###

* This repository contains the source code to collect images from Flickr that are tagged or the title contains the following structure: [Number] + [Ordinal] + ["Birthday"]
* Version 1.0

### How do I get set up? ###

* This project is easy to get setup. Make sure MongoDB is installed
* Clone the repository with git
* Edit the configuration.py file
* Run the dependencies by executing pip install -r requirements.txt
* In order to run a test with the first script, execute python run_metadata.py 30
* In order to run a test with the second script, execute python run_download.py 30

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Felix Anda
* https://forensicsandsecurity.com/