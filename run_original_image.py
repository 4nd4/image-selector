# run through image_created table

# get url and download original image

# replace previous image with original

# get number records
from configuration import yahoo_db


def update_original_image():

    pipe_condition = {'original': {'$exists': False}}
    pipe_condition_update = pipe_condition

    num_records_db = yahoo_db.get_cl_image_created().count(pipe_condition)

    counter = 0

    while counter < num_records_db:

        image_record = yahoo_db.get_cl_image_created().find_one(
            {
                pipe_condition
            }
        )

        subject_id = image_record.get('id')

        if subject_id is not None:
            if yahoo_db.create_file(record=image_record, original=True):
                print('file created')

                pipe_condition_update.update({
                    'id': subject_id
                })

                update_record = yahoo_db.get_cl_image_created().update_one(
                    pipe_condition_update, {'$set': {'original': True}}
                )

                print(update_record)

            else:
                print('file not created')

        counter += 1


if True:
    update_original_image()
