import os
import configuration
import sys
from face_detector.cnn import cnn_face_detector
from face_detector.fi_functions import copy_file
from face_detector.hog import hog_face_detector

pre_trained_model = os.path.expanduser(configuration.cnn_pretrained_model_path)
yahoo_db = configuration.yahoo_db


def detect_faces_db(age_default):

    if len(sys.argv) == 1:
        age_param = age_default
        print('executing default age', age_default)

    else:
        age_param = int(sys.argv[1])

    number_records = yahoo_db.count_image_face_recognition(age_param)

    print('processing', {
        'number_records': number_records, 'age': age_param
    }
          )

    count_images = 0

    try:

        while count_images != number_records:
            image_record = yahoo_db.get_record_image_face_recognition(age_param)

            if image_record is not None and '_id' in image_record:
                subject_id = image_record['_id']

                full_path = configuration.path_directory_creation + "{0}/{1}.{2}".format(
                    str(age_param),
                    str(subject_id),
                    configuration.image_extension
                )

                if os.path.exists(full_path):

                    list_result_hog = hog_face_detector(full_path)
                    list_result_cnn = cnn_face_detector(pre_trained_model, full_path)

                    pipe = {
                        'face_recognition':
                            {
                                'hog_dlib': list_result_hog,
                                'cnn_dlib': list_result_cnn
                            }
                    }

                    # execute facial recognition

                    # insert pipe to db

                    yahoo_db.insert_face_recognition(subject_id, pipe)

                    count_images += 1

                    print('inserted', count_images)

                else:
                    print('record was not found on disk')
                    # delete?
    except KeyboardInterrupt:
        print('interrupted')


def detect_faces(age_default, copy=None):
    if len(sys.argv) == 1:
        age_param = age_default
        print('executing default age', age_default)

    else:
        age_param = int(sys.argv[1])

    directory_path = configuration.path_directory_creation + str(age_param)

    # go to the age folder and start iterating each image,
    # run the face detector and record the faces on the database
    # use face_detector_hog and face_detector_cnn
    # if faceRectangle doesn't exists, iterate face detection

    try:

        if not os.path.exists(directory_path):
            os.mkdir(directory_path)

        for item in os.listdir(directory_path):
            if os.path.isfile(os.path.join(directory_path, item)):
                try:

                    if not item[0] == '.':

                        # check if it has been evaluated before (although by moving files to a different folder,
                        #  it should not happen

                        subject_id = os.path.splitext(item)[0]

                        face_recognition_record = yahoo_db.check_face_recognition(subject_id)

                        full_path = directory_path + "/" + item

                        if not face_recognition_record:

                            list_result_hog = hog_face_detector(full_path)
                            list_result_cnn = cnn_face_detector(pre_trained_model, full_path)

                            pipe = {
                                'face_recognition':
                                    {
                                        'hog_dlib': list_result_hog,
                                        'cnn_dlib': list_result_cnn
                                    }
                            }

                            # execute facial recognition

                            # insert pipe to db

                            yahoo_db.insert_face_recognition(subject_id, pipe)

                            # todo: copy file to folder adequately

                            # check cnn_dlib

                            number_faces = len(pipe['face_recognition']['cnn_dlib'])

                            if copy:
                                copy_file(age_param, full_path, number_faces)

                        else:
                            print('Already inserted previously to the database', 'attempting to copy file')
                            # try copying

                            # check file from db and create in folder

                            # face_recognition (cnn_dlib plus hog_dlib)
                            # age
                            # file_path

                            if 'face_recognition' in face_recognition_record:
                                face_recognition = face_recognition_record.get('face_recognition')
                                if 'cnn_dlib' in face_recognition:
                                    number_faces = len(face_recognition.get('cnn_dlib'))

                            if copy:
                                copy_file(age_param, full_path, number_faces)

                except Exception as e:
                    print('error', e)

    except KeyboardInterrupt:

        # try complete process

        print('interrupted')


#################################################################
# run code  #
#################################################################

# __MAIN__

# generates images on db and physical


if True:
    detect_faces(1, True)

if False:
    detect_faces_db(30)

# make the Pool of workers

if False:
    pool = ThreadPool(8)
    pool.map(detect_faces_db, range(1, 20))


print('Done')
