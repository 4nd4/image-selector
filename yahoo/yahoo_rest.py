import random
import urllib
import os
from model import fi_functions
from model.db_image import Database
from model.fi_functions import calculate_md5


class YahooRest(Database):
    def __init__(self):
        Database.__init__(self, "YAHOO_REST")

    def set_image_path(self, record):

        temp_folder = self.get_root_path()

        url_path = os.path.join(temp_folder, str(record.get('age')), str(record.get('_id')) + '.' + record.get('originalformat'))

        self.image_path = url_path

    def _get_record_metadata(self, _id):

        return self.get_cl_data().find_one({'_id': _id})

    def _get_single_record_metadata(self, age):

        list_deleted = self.get_deleted_list()
        list_processed = self.get_processed_list()
        list_filtered = list_deleted + list_processed

        pipe = {
            'age': age,
            '_id': {"$nin": list_filtered}
        }

        return self.get_cl_data().find_one(pipe)

    def _get_record(self, age):
        list_deleted = self.get_deleted_list()
        list_processed = self.get_processed_list()

        list_filtered = list_deleted + list_processed

        pipe = {
            'age': age,
            '_id': {"$nin": list_filtered},
        }

        return self.get_cl_data().find(pipe)

    def _get_single_random_record(self, age):

        count = self._get_record(age).count()

        if count > 0:

            return self._get_record(age)[random.randrange(count)]

    def count_images(self, age):

        image_count = self._get_record(age).count()

        return image_count

    def get_metadata_image(self, age):

        image = self._get_single_record_metadata(age)

        if image is not None:

            temp_folder = self.get_root_path() + str(age)

            if image.get('urls') is None:
                # delete image_created and data
                print('deleting ...')
                self.cl_image_created.remove({'id': image.get('_id')})
                self.cl_data.remove({'_id': image.get('_id')})

            if image.get('originalformat') is not None:
                extension = image.get('originalformat')
            else:
                extension = 'jpg'   # in the past, the files were all jpg

            url_path = os.path.join(temp_folder, str(image.get('_id')) + '.' + extension)

            image.update({'full_path_url': url_path})

            id_metadata = image.get('_id')

            pipe = {'id': id_metadata, 'age': age}

            # create file physically

            if self.create_file(image):

                # prevent multiple inserts

                if not self.get_cl_image_created().find_one(pipe):

                    self.get_cl_image_created().insert(
                        pipe
                    )
                else:
                    print('record already inserted', id_metadata)

                return image

            else:

                pipe.update({'not_available': True})

                if not self.get_cl_image_created().find_one(pipe):

                    self.get_cl_image_created().insert(
                        pipe
                    )
                else:
                    print('unavailable record already inserted', id_metadata)

                return {}

    def get_metadata_download(self, _id):

        image = self._get_record_metadata(_id)

        if image is not None:

            age = image.get('age')
            url = image.get('url_o')
            extension = os.path.splitext(url)[1]

            temp_folder = self.get_root_path() + str(age)

            url_path = os.path.join(temp_folder, str(_id) + extension)

            # url_path = os.path.join(temp_folder, str(_id) + '.' + configuration.image_extension)

            image.update({'full_path_url': url_path})

            pipe = {'id': _id, 'age': image.get('age')}

            # create file physically

            if self.create_file(image):

                if not self.get_cl_image_created().find_one(pipe):

                    self.get_cl_image_created().insert(pipe)
                else:
                    print('record already inserted', _id)

                # insert md5 in data collection

                image_md5 = calculate_md5(url_path)

                # check if the md5 already exists

                count_md5_records = self.get_cl_data().find({'md5': image_md5}).count()
                md5_records = self.get_cl_data().find({'md5': image_md5})

                print('md5 duplicates :', count_md5_records)

                if count_md5_records == 0:

                    self.get_cl_data().update_one(
                        {
                            '_id': _id
                        },
                        {
                            '$set': {'md5': image_md5}
                        }
                    )
                else:
                    for r in md5_records:
                        print('md5 already in database', r)

                        if r.get('title') is None:
                            # update record and delete

                            '''
                            self.get_cl_data().update_one(
                                {
                                    '_id': r.get('_id')
                                },
                                {
                                    '$set': {
                                        'dateuploaded': image.get('dateuploaded'),
                                        'owner': image.get('owner'),
                                        'id': image.get('id'),
                                        'title': image.get('title'),
                                        'description': image.get('description'),
                                        'dates': image.get('dates'),
                                        'tags': image.get('tags'),
                                        'url_o': image.get('url_o'),
                                        'urls': image.get('urls')
                                    }
                                }
                            )
                            
                            '''
                            update_pipe = {'_id': r.get('_id')}

                            image.update({'_id'})

                            self.get_cl_data().update_one(
                                update_pipe,
                                {
                                    '$set': image
                                }
                            )

                            break

                return image
            
            else:

                pipe.update({'not_available': True})

                if not self.get_cl_image_created().find_one(pipe):

                    self.get_cl_image_created().insert(pipe)
                else:
                    print('unavailable record already inserted', _id)   #todo

                return {}

        else:
            print('no record on data db')

    def get_random_image(self, age):

        while True:

            image = self._get_single_random_record(age)

            if image is None:
                break

            temp_folder = self.get_root_path()

            url_path = os.path.join(temp_folder, str(image['_id']) + '.jpg')

            image.update({'full_path_url': url_path})

            id_random = image['_id']

            # create file physically

            if self.create_file(image):

                self.get_random().insert(
                    {'id': id_random}
                )

                return image

            else:

                # remove the file

                self.get_delete().insert(
                    {'id': id_random}
                )

    def manage_paths(self, record):

        farm = record.get('farm')
        server = record.get('server')
        photo_id = record.get('id')
        secret = record.get('secret')
        size = 'o'  # TODO: get sizes

        url = _construct_url(farm, server, photo_id, secret, size)

        return url

    def create_file(self, record, original=None):

        url = record.get('url_o')
        age = record.get('age')
        #extension = os.path.splitext(url)[1]

        # check if path already has file

        temp_folder = self.get_root_path() + str(age)

        if not os.path.exists(temp_folder) or original:
            print('creating directory...')
            os.mkdir(temp_folder)

        #url_path = os.path.join(temp_folder, str(record.get('_id')) + extension)

        url_path = record.get('full_path_url')

        temp_file = urllib.URLopener()

        try:

            temp_file.retrieve(url, url_path)
            temp_file.close()

        except IOError as ie:
            print('IO Error - No photo', ie)

            if ie.args[0] == 'http error' and ie.args[1] == 302:
                print('no photo found', url)

            return False
            # try again

        except Exception as e:
            print(e)
            return False

        age = 'n/a'

        age_record = record.get('age')

        if age_record is not None:
            age = str(age_record)

        print(url_path, age)

        return True

    def create_images(self, age):

        fi_functions.delete_folder(self.get_root_path())

        images = self._get_record(age)

        for row in images:
            self.create_file(row)


def _construct_url(farm, server, photo_id, secret, size):
    return "https://farm{0}.staticflickr.com/{1}/{2}_{3}_{4}.jpg".format(
      farm,
      server,
      photo_id,
      secret,
      size
    )
