import os

from pymongo import MongoClient

import configuration
from face_detector.cnn import cnn_face_detector
from face_detector.hog import hog_face_detector
from multiprocessing.dummy import Pool as ThreadPool

pre_trained_model = os.path.expanduser(configuration.cnn_pretrained_model_path)
yahoo_db = configuration.yahoo_db

client = MongoClient()

database = client['YAHOO_REST']
cl_data = database['data']


def count_image_face_recognition():

    record = cl_data.find(
        {
            '$and': [{'age': {'$exists': True}}, {'id': {'$exists': False}}, {'face_recognition': {'$exists': False}}]
        }
    )

    return record.count()


def get_record_image_face_recognition():

    record = cl_data.find_one(
        {
            '$and': [{'age': {'$exists': True}}, {'id': {'$exists': False}}, {'face_recognition': {'$exists': False}}]
        }
    )

    return record


def detect_faces_db():

    number_records = count_image_face_recognition()

    print('processing', {
        'number_records': number_records
    }
          )

    count_images = 0

    try:

        while count_images != number_records:
            image_record = get_record_image_face_recognition()

            if image_record is not None and '_id' in image_record:
                subject_id = image_record['_id']

                full_path = configuration.path_directory_creation + "{0}/{1}.{2}".format(
                    str(image_record.get('age')),
                    str(subject_id),
                    configuration.image_extension
                )

                if os.path.exists(full_path):

                    list_result_hog = hog_face_detector(full_path)
                    list_result_cnn = cnn_face_detector(pre_trained_model, full_path)

                    pipe = {
                        'face_recognition':
                            {
                                'hog_dlib': list_result_hog,
                                'cnn_dlib': list_result_cnn
                            }
                    }

                    # execute facial recognition

                    # insert pipe to db

                    yahoo_db.insert_face_recognition(subject_id, pipe)

                    count_images += 1

                    print('inserted', count_images)

                else:
                    print('record was not found on disk')
                    # delete?
    except KeyboardInterrupt:
        print('interrupted')


#################################################################
# run code  #
#################################################################

# __MAIN__

# generates images on db and physical


if True:
    detect_faces_db()

# make the Pool of workers

if False:
    pool = ThreadPool(8)
    pool.map(detect_faces_db, range(1, 20))


print('Done')
