from bson import ObjectId

from model.db_image import Database


def update_deleted_files():

    db = Database('YAHOO_REST')

    '''
    pipe2 = {
        {
            '$group': {
                '_id': {
                    'id': '$id'
                },
                'uniqueIds': {'$addToSet': '$_id'},
                'count': {'$sum': 1}
            }
        },
        {
            '$match': {
                'count': {'$gt': 1}
            }
        },
        #{
        #    '$unwind': {'$uniqueIds'}
        #}
    }
    '''

    pipeline = [
        #{"$unwind": "$_id"},
        {'$match': {'id': {'$exists': False}}},
        {
            "$group": {

                '_id': {
                    "_id": "$id"},
                    'uniqueIds': {'$addToSet': '$_id'},
                    "count": {"$sum": 1}
                }
        }
    ]

    result = db.get_cl_data().aggregate(
        pipeline
    )

    for res in result:
        for i in res.get('uniqueIds'):

            db.get_cl_data().update_one(
                {
                    '_id': i,

                },
                {
                    '$set': {'id': ObjectId(i)}
                },
                upsert=False
            )


update_deleted_files()
print('done')