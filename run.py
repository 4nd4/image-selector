import calendar
import datetime
import os
import sys
import time

import configuration
from birthday_dictionary import filter_one_dictionary
from model.db_image import Database
from model.fi_functions import calculate_md5
from model.photos import Photos
from run_metadata import get_ordinal_indicator

p = Photos()


def fix_deleted_metadata(age_default, download):

    if len(sys.argv) == 1:
        start_age = age_default
        print('using default age because you sent no parameter', age_default)
    else:
        start_age = int(sys.argv[1])

    try:

        values = filter_one_dictionary[start_age]

        print('#' * 20)
        print('age', start_age)

        # today
        print('proceeding to fix ......')

        epoch_now = int(time.time())

        time_window = 86400 * 1    # check with a frame of 1 day
        dt = datetime.datetime(2000, 01, 01, 0, 0)
        min_unixtime = calendar.timegm(dt.utctimetuple())

        readable_epoch_now = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(epoch_now))
        readable_epoch_to = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(min_unixtime))

        print('attempting to retrieve records from {0} to {1}'.format(readable_epoch_now, readable_epoch_to))

        epoch_before = epoch_now - time_window

        ranges = [epoch_before, epoch_now]

        # 0 is all rights reserved

        licenses = '1,2,3,4,5,6,7,8,9,10'

        ordinal_indicator = get_ordinal_indicator(values)

        birthday_text = ordinal_indicator + " " + 'birthday'

        while epoch_now > min_unixtime:

            print(readable_epoch_now)

            # store date in log for age

            p.search(
                age=start_age,
                text=birthday_text,
                ranges=ranges,
                license=licenses,
                media='photos',
                extras='url_o',
                page=1,
                download=download
            )

            epoch_now = ranges[0]
            epoch_before = ranges[0] - time_window

            ranges = [epoch_before, epoch_now]

    except KeyboardInterrupt:
        print('interrupted')


def calculate_md5_images(age_default):

    if len(sys.argv) == 1:
        age = age_default
        print('using default age because you sent no parameter', age_default)
    else:
        age = int(sys.argv[1])

    d = Database('YAHOO_REST')

    pipe = {'age': age, 'md5': {'$exists': False}}

    number_to_process = d.get_cl_image_created().find(pipe).count()

    counter = 0

    while counter < number_to_process:

        record_download = d.get_cl_image_created().find_one(pipe)

        # get path

        path = None

        if record_download:
            subject_id = record_download.get('id')

            if subject_id is not None:
                path = os.path.join(
                    configuration.path_directory_creation,
                    str(age),
                    str(subject_id) + '.' + configuration.image_extension
                )

            if os.path.exists(path):

                # calculate hash

                md5_hash = calculate_md5(path)

                d.get_cl_image_created().update_one(
                    {
                        'id': subject_id
                    },
                    {
                        '$set': {'md5': md5_hash}
                    }
                )

                print('md5 calculated for record', subject_id)

            else:
                print('file not found', path)

        counter += 1


if False:
    fix_deleted_metadata(age_default=1, download=True)

if True:
    calculate_md5_images(age_default=2)

print('done')
